.. fab-access documentation master file, created by
   sphinx-quickstart on Thu Jul 16 19:46:22 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FabAccess documentation!
======================================

FabAccess is still in a state of development and has no stable Release.

So there is no stability and backward compatibility between the versions.
Feel free to help us testing our software stack but not expect a full functional software.
We will inform you about breaking changes over our Zulip. Please subscript to it to stay up to date, because the server and client can get incompatible.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation/installation.rst
   configuration/configuration.rst
   usage/usage.rst
